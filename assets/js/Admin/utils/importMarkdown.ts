function paragraph(text: string) {
  if (text.match(/^(\r?\n)+$/)) return null;
  return {
    type: "paragraph",
    children: [{ text }]
  };
}

export default function importMarkdown(text: any) {
  if (!text || typeof text !== "string") return [];
  return text;
  return text
    .split(/(\r\n|\r|\n){2,}/)
    .map(paragraph)
    .filter(Boolean);
}
