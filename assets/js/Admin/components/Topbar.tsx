import React from "react";
import { NavLink } from "react-router-dom";
const HomeIcon = require("../icons/home.svg").default;
const WordpressIcon = require("../icons/wordpress.svg").default;
const PlusIcon = require("../icons/plus.svg").default;

interface ITobparLinkProps {
  text?: string;
  to: string;
  className?: string;
  icon?: React.FunctionComponent<React.SVGAttributes<SVGElement>>;
}

const TopbarLink = ({ icon: Icon, className, text, to }: ITobparLinkProps) => (
  <NavLink className={`admin_zone__link ${className || ""}`} to={to}>
    <Icon />
    {text}
  </NavLink>
);

export default () => {
  return (
    <nav className="admin_zone">
      <TopbarLink
        to="javascript:void"
        className="admin_zone__wordpress"
        icon={WordpressIcon}
      />
      <a href="/" className="admin_zone__link">
        <HomeIcon />
        2137.io
      </a>
      <TopbarLink to="/admin/posts/new" text="New" icon={PlusIcon} />
      <span className="admin_zone__username">Howdy, Karol Moroz</span>
      <a className="admin_zone__link" href="/sign-out">
        Sign out
      </a>
    </nav>
  );
};
