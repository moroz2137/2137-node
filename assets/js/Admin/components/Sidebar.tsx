import React from "react";
import { NavLink } from "react-router-dom";
const ThumbtackIcon = require("../icons/thumbtack.svg").default;

interface ISideNavLinkProps {
  text?: string;
  to: string;
  className?: string;
  icon?: React.FunctionComponent<React.SVGAttributes<SVGElement>>;
}

const SideNavLink = ({
  icon: Icon,
  className,
  text,
  to
}: ISideNavLinkProps) => (
  <NavLink
    className="admin_sidebar__link"
    to={to}
    activeClassName="admin_sidebar__link--active"
  >
    {Icon && <Icon />}
    {text}
  </NavLink>
);

export default () => (
  <nav className="admin_sidebar">
    <SideNavLink text="Posts" to="/" icon={ThumbtackIcon} />
  </nav>
);
