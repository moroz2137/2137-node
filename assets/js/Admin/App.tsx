import React from "react";
import Topbar from "./components/Topbar";
import Sidebar from "./components/Sidebar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PostList from "./views/PostList";
import EditPost from "./views/EditPost";

export default () => (
  <Router basename="/wp-admin">
    <div className="admin_app">
      <Topbar />
      <Sidebar />
      <main>
        <Switch>
          <Route path="/" exact component={PostList} />
          <Route path="/posts/:slug" exact component={EditPost} />
        </Switch>
      </main>
    </div>
  </Router>
);
