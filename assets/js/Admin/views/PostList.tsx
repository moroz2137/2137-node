import React from "react";
import { LIST_POSTS } from "../graphql/queries/postQueries";
import day from "dayjs";
import qs from "querystring";
import { useHistory } from "react-router-dom";
import useQuery from "../graphql/useQuery";

interface Post {
  id: string | number;
  title: string;
  slug: string;
  number: number;
  insertedAt: string;
  updatedAt: string;
}

const PostTableHeader = () => (
  <thead>
    <tr>
      <th className="post_number">#</th>
      <th className="post_title">Title</th>
      <th className="post_categories">Categories</th>
      <th className="post_date">Date</th>
    </tr>
  </thead>
);

const PostRow = ({ post }: { post: Post; key: any }) => {
  const history = useHistory();
  return (
    <tr onClick={() => history.push(`/posts/${post.slug}`)}>
      <td className="post_number">{post.number}</td>
      <td className="post_title">{post.title}</td>
      <td className="post_categories">&mdash;</td>
      <td className="post_date">{day(post.insertedAt).format("YYYY-MM-DD")}</td>
    </tr>
  );
};

const PostTable = ({ data }: { data: Post[] }) => (
  <table className="table is-bordered is-hoverable post_table">
    <PostTableHeader />
    <tbody>
      {data.map(post => (
        <PostRow post={post} key={post.id} />
      ))}
    </tbody>
    <PostTableHeader />
  </table>
);

const getPageFromHistory = (history: any) => {
  const { search } = history?.location;
  if (!search) return;
  const { page } = qs.decode(search.replace(/^\?/, ""));
  return parseInt(page as any);
};

export default ({ history }) => {
  const page = getPageFromHistory(history);
  const { data, loading } = useQuery(LIST_POSTS, { page });
  return (
    <div className="post_list">
      <h3 className="title is-3">Post list</h3>
      {loading ? <p>Loading...</p> : <PostTable data={data.posts?.entries} />}
    </div>
  );
};
