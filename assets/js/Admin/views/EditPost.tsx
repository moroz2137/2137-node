import React, { useMemo, useState, useEffect } from "react";
import useQuery from "../graphql/useQuery";
import { GET_POST } from "../graphql/queries/postQueries";
import { createEditor } from "slate";
import { Slate, Editable, withReact } from "slate-react";
import importMarkdown from "../utils/importMarkdown";

export default ({ match }) => {
  const { data, loading } = useQuery(GET_POST, { slug: match?.params?.slug });
  const [editorState, setEditorState] = useState([]);
  const editor = useMemo(() => withReact(createEditor()), []);
  useEffect(() => {
    const newState = importMarkdown(data?.post?.body);
    if (newState.length) setEditorState(newState);
  }, [data, loading]);
  if (loading) return <p>Loading...</p>;
  const post = data.post;
  return (
    <div className="edit_post">
      <h1 className="title">
        {post.number}. {post.title}
      </h1>
      <Slate
        editor={editor}
        value={editorState}
        onChange={value => setEditorState(value)}
      >
        <Editable />
      </Slate>
    </div>
  );
};
