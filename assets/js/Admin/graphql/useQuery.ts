import { useState, useEffect, useRef } from "react";
import client from "./client";
import isEqual from "lodash/isEqual";

export default (query: string, variables: Object = {}) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  // https://stackoverflow.com/questions/54095994/react-useeffect-comparing-objects
  function useDeepCompareMemoize(value) {
    const ref = useRef();
    if (!isEqual(value, ref.current)) {
      ref.current = value;
    }
    return ref.current;
  }

  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      try {
        const data = await client.query(query, variables);
        setData(data);
        setError(null);
      } catch (err) {
        setData(null);
        setError(err);
      }
      setLoading(false);
    }
    fetchData();
  }, useDeepCompareMemoize([variables]));

  return {
    data,
    loading,
    error
  };
};
