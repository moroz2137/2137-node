export const LIST_POSTS = `
  query listPosts($page: Int = 1) {
    posts(page: $page) {
      cursor {
        page
        totalPages
      }
      entries {
        id
        slug
        title
        number
        insertedAt
        publishedAt
      }
    }
  }
`;

export const GET_POST = `
  query post($slug: ID!) {
    post(id: $slug) {
      id
      slug
      title
      number
      publishedAt
      insertedAt
      updatedAt
      body
    }
  }
`;
