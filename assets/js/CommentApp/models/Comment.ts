import gql from "../client/graphql";
import { FETCH_COMMENTS, CREATE_COMMENT } from "../client/queries";
type id = number | string;

export interface Comment {
  id: id;
  body: string;
  signature: string;
  website: string | null;
  ip: string | null;
  postId: id;
  insertedAt: string;
  updatedAt: string;
}

export interface CommentParams {
  postId: id;
  signature: string;
  body: string;
}

export default class Comments {
  static async fetchComments(postId: id) {
    const { comments } = await gql.query(FETCH_COMMENTS, { postId });
    return comments || [];
  }

  static async createComment(params: CommentParams) {
    const { createComment } = await gql.query(CREATE_COMMENT, { params });
    return createComment;
  }
}
