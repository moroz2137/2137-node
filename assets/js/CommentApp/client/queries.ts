export const COMMENT_DETAILS = `
fragment CommentDetails on Comment {
  id body signature website
  ip insertedAt
}`;

export const FETCH_COMMENTS = `
${COMMENT_DETAILS}
query comments($postId: ID!) {
  comments(postId: $postId) {
    ...CommentDetails
  }
}`;

export const CREATE_COMMENT = `
${COMMENT_DETAILS}
mutation createComment($params: CommentParams!){
  createComment(params:$params) {
    success
    comment { ...CommentDetails }
    errors
  }
}
`;
