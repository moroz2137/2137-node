import Vue from "vue";
import Main from "./components/Main.vue";

Vue.config.devtools = true;
document.addEventListener("DOMContentLoaded", () => {
  const container = document.getElementById("commentApp");
  if (!container) return;
  const postId = container.dataset.postId;
  new Vue({
    render: createEl => createEl(Main, { props: { postId } })
  }).$mount("#commentApp");
});
