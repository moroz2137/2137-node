import "highlight.js/styles/tomorrow.css";
import "../css/style.sass";
import round from "lodash/round";

const setDocumentHeight = () => {
  const body = document.body,
    html = document.documentElement;

  window.documentHeight = Math.max(
    body.scrollHeight,
    body.offsetHeight,
    html.clientHeight,
    html.scrollHeight,
    html.offsetHeight
  );
};

const getInitial = (el, property) => {
  const str = window.getComputedStyle(el).getPropertyValue(property);
  return parseFloat(str);
};

const setParallaxEffect = () => {
  setDocumentHeight();
  const parallaxDiv = document.getElementById("parallax");
  const startPosition = getInitial(parallaxDiv, "background-position-y");
  const startHeight = getInitial(parallaxDiv, "height");
  document.querySelectorAll(".post-content img").forEach(img => {
    img.addEventListener("load", setDocumentHeight);
  });
  window.addEventListener("resize", setDocumentHeight);
  window.addEventListener("scroll", e => {
    // how many percentiles down the background should scroll
    const positionDiff = 45;
    const heightDiff = 0.05 * window.innerHeight;

    // values of window.scrollY when parallax is triggered
    // e.g. it starts scrolling 150px down the page
    // and stops 150px from the bottom of the document
    const startScrollY = 30;
    const endScrollY = window.documentHeight - startScrollY;

    // y === scrolling offset relative to startScrollY
    const maxY = endScrollY - startScrollY;
    const y = window.scrollY - startScrollY;

    if (y < 0 || y > maxY) return;

    let newPosition = round((y / maxY) * positionDiff + startPosition, 2);
    let newHeight = round((y / maxY) * heightDiff + startHeight, 2);
    parallaxDiv.style.backgroundPositionY = `${newPosition}%`;
    parallaxDiv.style.height = `${newHeight}px`;
  });
};

document.addEventListener("DOMContentLoaded", () => {
  if (window.innerWidth >= 500) setParallaxEffect();
});
