import express from "express";
import markdown from "./util/markdown";
import BlogHelper from "./util/BlogHelper";
import BlogController from "./controllers/BlogController";
import PageController from "./controllers/PageController";
import SessionController from "./controllers/SessionController";

const router = express.Router();

router.use((req, res, next) => {
  res.locals.markdown = markdown;
  res.locals.blog = BlogHelper;
  next();
});

router.get("/", PageController.index);
router.get("/blog", BlogController.index);
router.get("/blog/:slug", BlogController.show);

router.get("/wp-login.php", SessionController.new);
router.post("/wp-login.php", SessionController.create);
router.get("/sign-out", SessionController.delete);

export default router;
