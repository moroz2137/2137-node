import path from "path";
process.env["NODE_CONFIG_DIR"] = path.resolve(__dirname, "../config");

import express from "express";
import config from "config";
import logger from "morgan";
import createError from "http-errors";
import helmet from "helmet";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import session from "express-session";
import flash from "express-flash";

import sequelize from "./sequelize";
import router from "./router";
import Endpoint from "./endpoint";
import publicGqlServer from "./graphql/public";
import authenticatedGqlServer from "./graphql/authenticated";
import fetchUser from "./util/fetchUser";
import AdminController from "./controllers/AdminController";
import { denyAccessWeb, denyAccessApi } from "./util/denyAccess";

const app = express();

app.use(helmet());
if (process.env.NODE_ENV === "production") {
  app.use(logger("combined"));
} else {
  app.use(logger("dev"));
}

const sessionStore = new session.MemoryStore();

app.use(express.static(path.join(__dirname, "../public")));

app.set("view engine", "pug");
app.set("views", path.join(__dirname, "/views"));
app.set("trust proxy", 1); // The website is running behind Nginx
publicGqlServer.applyMiddleware({ app, path: "/api/public" });
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(
  session({
    cookie: { maxAge: 60000 },
    store: sessionStore,
    secret: config.get("secrets.secretKeyBase"),
    resave: true,
    saveUninitialized: true
  })
);
app.use(flash());
app.use(fetchUser, router);
app.get("/wp-admin*", denyAccessWeb, AdminController.app);
app.get("/wp-admin", denyAccessWeb, AdminController.app);
app.use(denyAccessApi);
authenticatedGqlServer.applyMiddleware({ app, path: "/api/authenticated" });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

sequelize(config.get("dbConfig"))
  .authenticate()
  .then(() => {
    Endpoint.startHttp(app);
  });
