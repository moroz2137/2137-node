import {
  Table,
  Column,
  Model,
  CreatedAt,
  UpdatedAt,
  DataType,
  AllowNull,
  ForeignKey,
  BelongsTo,
  BeforeValidate
} from "sequelize-typescript";
import Post from "./Post";

@Table({ underscored: true })
export default class Comment extends Model<Comment> {
  @Column({
    allowNull: false,
    validate: {
      len: {
        args: [10, 1000],
        msg: "Please enter 10-1000 characters."
      }
    },
    unique: "comments_body_post_id_index"
  })
  body!: string;

  @Column({
    allowNull: false,
    validate: {
      len: {
        args: [2, 50],
        msg: "Please enter at least 2 characters."
      }
    }
  })
  signature!: string;

  @AllowNull
  @Column
  website?: string;

  @Column({ type: DataType.INET, allowNull: true })
  ip?: string;

  @ForeignKey(() => Post)
  @Column({
    allowNull: false,
    validate: {
      notEmpty: true
    }
  })
  postId!: number;

  @BelongsTo(() => Post)
  post!: Post;

  @CreatedAt
  insertedAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BeforeValidate
  static async trimBody(instance: Post) {
    if (typeof instance.body === "string") {
      instance.body = instance.body.trim();
    }
  }
}
