import {
  Table,
  Column,
  Model,
  CreatedAt,
  UpdatedAt,
  addOptions
} from "sequelize-typescript";
import { FindOptions } from "sequelize/types";
import Sequelize from "sequelize";
const Op = Sequelize.Op;
const PAGE_SIZE = 27;

@Table({ underscored: true })
class Post extends Model<Post> {
  @Column
  title!: string;

  @Column
  body!: string;

  @Column
  number!: number;

  @Column
  slug!: string;

  @Column
  language!: string;

  @Column
  publishedAt!: Date;

  @Column
  viewCount!: number;

  @CreatedAt
  insertedAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  incrementViews() {
    this.increment("viewCount");
    this.save();
  }

  static async page(page: number = 1, opts: FindOptions = {}) {
    const offset = (page - 1) * PAGE_SIZE;

    const entries = this.findAll({
      order: [["number", "DESC"]],
      where: { [Op.not]: { publishedAt: null } },
      limit: PAGE_SIZE,
      offset,
      ...opts
    });

    const count = await this.count({
      where: { [Op.not]: { publishedAt: null } }
    });

    return {
      count,
      totalPages: Math.ceil(count / PAGE_SIZE),
      entries: await entries,
      page
    };
  }

  static all(opts: FindOptions = {}) {
    return this.findAll({
      order: [["number", "DESC"]],
      where: { [Op.not]: { publishedAt: null } },
      ...opts
    });
  }

  static findBySlug(slug: string) {
    return this.findOne({ where: { slug } });
  }
}

export default Post;
