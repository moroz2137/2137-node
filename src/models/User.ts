import {
  Table,
  Column,
  Model,
  CreatedAt,
  UpdatedAt,
  DataType,
  AllowNull,
  BeforeUpdate,
  BeforeCreate,
  Unique
} from "sequelize-typescript";
import bcrypt from "bcrypt";
import Token from "../util/Token";

const BCRYPT_ROUNDS = 10;

@Table({ underscored: true })
export default class User extends Model<User> {
  @Unique
  @Column({
    validate: {
      isEmail: true
    }
  })
  email!: string;

  @Column
  passwordHash!: string;

  @Column({
    type: DataType.VIRTUAL,
    validate: {
      len: {
        args: [8, 64],
        msg: "Password must be between 8 and 64 characters"
      }
    }
  })
  password?: string;

  @Column(DataType.VIRTUAL)
  passwordConfirmation?: string;

  @Column({ allowNull: false })
  displayName!: string;

  @CreatedAt
  insertedAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  static findByEmail(email: string) {
    return User.findOne({ where: { email } });
  }

  static async findByJwt(token: string) {
    const payload = await Token.verify(token);
    if (!payload) return null;
    return User.findOne({ where: { id: payload.sub } });
  }

  static async authenticateByEmailPassword(email: string, password: string) {
    const user = await User.findByEmail(email);
    if (!user) return null;
    if (await bcrypt.compare(password, user.passwordHash)) {
      return user;
    } else {
      return null;
    }
  }

  @BeforeCreate
  @BeforeUpdate
  static async hashPassword(instance: User) {
    if (instance.password) {
      instance.passwordHash = await bcrypt.hash(
        instance.password,
        BCRYPT_ROUNDS
      );
    }
  }
}
