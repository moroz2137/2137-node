import Express from "express";
import https from "https";
import fs from "fs";
import path from "path";

export default class Endpoint {
  static startHttp(app: Express.Application, port = 3000) {
    app.listen(port, () => {
      console.log(`HTTP endpoint starting at port ${port}...`);
    });
  }

  static startHttps(app: Express.Application, certDir: string, port = 3001) {
    const cert = fs.readFileSync(path.resolve(certDir, "server.cert"));
    const key = fs.readFileSync(path.resolve(certDir, "server.key"));
    https.createServer({ key, cert }, app).listen(port, () => {
      console.log(`HTTPS endpoint starting at port ${port}...`);
    });
  }
}
