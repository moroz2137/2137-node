import sequelize from "../sequelize";
import config from "config";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
chai.use(chaiAsPromised);

(async () => {
  console.log("Syncing test database...");
  await sequelize(config.get("dbConfig")).sync({ logging: false, force: true });
  run();
})();
