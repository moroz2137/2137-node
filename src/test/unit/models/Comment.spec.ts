import "../../testHelpers";

import { expect } from "chai";
import factory from "../../factories";
import "mocha";

function expectValidationError(promise: Promise<any>) {
  expect(promise).to.eventually.be.rejectedWith(Error);
}

describe("Comment", () => {
  it("saves with correct parameters", async () => {
    factory.create("comment");
  });

  it("does not save without body", async () => {
    const promise = factory.create("comment", { body: "" });
    expectValidationError(promise);
  });

  it("does not save without signature", async () => {
    const promise = factory.create("comment", { signature: "" });
    expectValidationError(promise);
  });

  it("does not save without postId", async () => {
    const promise = factory.create("comment", { postId: null });
    expectValidationError(promise);
  });
});
