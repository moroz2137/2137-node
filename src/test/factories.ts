const FactoryGirl = require("factory-girl");
import Comment from "../models/Comment";
import Post from "../models/Post";
import Chance from "chance";
const chance = new Chance();
const factory = FactoryGirl.factory;
const adapter = new FactoryGirl.SequelizeAdapter();

factory.setAdapter(adapter);

factory.define("post", Post, {
  title: factory.sequence("post-title", n => `Post #${n}`),
  body: chance.sentence(),
  number: factory.sequence("post-number"),
  slug: factory.sequence("post-slug", n => `post-no-${n}`),
  publishedAt: new Date(),
  language: "en"
});

factory.define("comment", Comment, {
  postId: factory.assoc("post", "id"),
  body: chance.sentence(),
  signature: chance.name()
});

export default factory;
