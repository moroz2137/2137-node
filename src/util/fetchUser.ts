import { Request, Response, NextFunction } from "express";
import User from "../models/User";

function fetchTokenFromCookies(req: Request) {
  return req.cookies.access_token;
}

function fetchBearerToken(req: Request) {
  return req.get("x-api-token");
}

export default async function fetchUser(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    res.locals.user = null;
    const token = fetchTokenFromCookies(req) || fetchBearerToken(req);
    const user = await User.findByJwt(token);
    if (user) {
      res.locals.user = user;
    }
  } catch (err) {
    console.error(err);
  }
  next();
}
