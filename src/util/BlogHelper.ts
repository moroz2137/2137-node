import Post from "../models/Post";
import markdown from "markdown-it";
import dayjs from "dayjs";

const renderer = markdown({
  html: false,
  typographer: true,
  linkify: true
});

export default class {
  static formatTitle(post: Post) {
    return `${post.number}. ${post.title}`;
  }
  static formatPostDate(date: Date | string) {
    return dayjs(date).format("D MMM., YYYY");
  }

  static formatLang({ language }: Post) {
    switch (language) {
      case "pl":
        return "Polish";
      case "en":
        return "English";
      case "zh":
        return "Chinese";
      case "ja":
      case "jp":
        return "Japanese";
    }
    return "Other";
  }

  static markdown(md: string) {
    return renderer.render(md);
  }
}
