import { Request, Response, NextFunction } from "express";

export function denyAccessApi(req: Request, res: Response, next: NextFunction) {
  if (res.locals?.user) {
    next();
  } else {
    res.status(401).end();
  }
}

export function denyAccessWeb(req: Request, res: Response, next: NextFunction) {
  if (res.locals?.user) {
    next();
  } else {
    req.flash("info", "Please sign in to proceed.");
    res.redirect("/wp-login.php");
  }
}
