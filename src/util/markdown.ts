import markdown from "markdown-it";
import highlight from "highlight.js";
import scss from "highlight.js/lib/languages/scss";

highlight.registerLanguage("sass", scss);
highlight.registerLanguage("graphql", <any>function(e: any) {
  return {
    aliases: ["gql"],
    keywords: {
      keyword:
        "query mutation subscription|10 type input schema directive interface union scalar fragment|10 enum on ...",
      literal: "true false null"
    },
    contains: [
      e.HASH_COMMENT_MODE,
      e.QUOTE_STRING_MODE,
      e.NUMBER_MODE,
      {
        className: "type",
        begin: "[^\\w][A-Z][a-z]",
        end: "\\W",
        excludeEnd: !0
      },
      {
        className: "literal",
        begin: "[^\\w][A-Z][A-Z]",
        end: "\\W",
        excludeEnd: !0
      },
      {
        className: "variable",
        begin: "\\$",
        end: "\\W",
        excludeEnd: !0
      },
      {
        className: "keyword",
        begin: "[.]{2}",
        end: "\\."
      },
      {
        className: "meta",
        begin: "@",
        end: "\\W",
        excludeEnd: !0
      }
    ],
    illegal: /([;<']|BEGIN)/
  };
});

const renderer = markdown({
  highlight: function(str, lang) {
    const stripped = lang && lang.replace("language-", "");
    if (stripped && highlight.getLanguage(stripped)) {
      try {
        return highlight.highlight(stripped, str).value;
      } catch (__) {}
    }

    return ""; // use external default escaping
  },
  html: true,
  typographer: true
});

export default function(md: string) {
  return renderer.render(md);
}
