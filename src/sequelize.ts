import { Sequelize } from "sequelize-typescript";
import path from "path";

interface DBConfig {
  user: string;
  password: string;
  host: string;
  database: string;
}

export default ({ database, user, password, host }: DBConfig) =>
  new Sequelize(database, user, password, {
    host: host,
    dialect: "postgres",
    models: [path.resolve(__dirname, "models")],
    logging: process.env.NODE_ENV === "test" ? false : console.log
  });
