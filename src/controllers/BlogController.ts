import Post from "../models/Post";
import BlogHelper from "../util/BlogHelper";

export default class BlogController {
  static async index(req, res, next) {
    try {
      const page = await Post.page(req.query.page);
      res.render("blog/index", { page });
    } catch (err) {
      next(err);
    }
  }

  static async show(req, res, next) {
    const post = await Post.findBySlug(req.params.slug);
    if (post) {
      const title = BlogHelper.formatTitle(post);
      post.incrementViews();
      res.render("blog/show", { post, title });
    } else {
      res.end("not found");
    }
  }
}
