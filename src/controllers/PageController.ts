export default class PageController {
  static index(req, res, next) {
    res.render("index.html.pug");
  }
}
