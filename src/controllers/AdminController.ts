import { Request, Response, NextFunction } from "express";

export default class AdminController {
  static app(req: Request, res: Response, next: NextFunction) {
    res.render("admin");
  }
}
