import config from "config";
import { Request, Response } from "express";
import User from "../models/User";
import Token from "../util/Token";

export default class SessionController {
  static new(req: Request, res: Response, next) {
    res.render("session/login");
  }

  static async create(req: Request, res: Response, next) {
    const { email, password } = req.body;
    const user = await User.authenticateByEmailPassword(email, password);
    if (user) {
      console.log(`Signed in as user ${user.email}`);
      res.cookie("access_token", Token.issue(user), {
        maxAge: config.get("session.maxAge"),
        httpOnly: true,
        secure: config.get("session.secure")
      });
      req.flash("success", `Welcome back, ${user.displayName}!`);
      res.redirect("/");
    } else {
      console.log(`Login failed for user ${email}.`);
      req.flash(
        "warning",
        "Login failed for the given email and password combination."
      );
      res.render("session/login");
    }
  }

  static delete(req: Request, res: Response, next) {
    res.clearCookie("access_token");
    return res.redirect("/");
  }
}
