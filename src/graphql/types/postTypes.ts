const { gql } = require("apollo-server");

const postTypes = gql`
  type Post {
    id: ID!
    number: Float!
    title: String!
    body: String!
    slug: String!
    publishedAt: DateTime
    insertedAt: DateTime!
    updatedAt: DateTime!
  }

  type Cursor {
    page: Int!
    totalPages: Int!
    totalEntries: Int!
  }

  type PostList {
    cursor: Cursor!
    entries: [Post!]!
  }
`;

export default postTypes;
