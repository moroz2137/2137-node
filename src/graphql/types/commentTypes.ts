const { gql } = require("apollo-server");

const commentTypes = gql`
  type Comment {
    id: ID!
    body: String!
    signature: String!
    website: String
    ip: String
    postId: ID!
    insertedAt: DateTime!
    updatedAt: DateTime!
  }
`;

export default commentTypes;
