import typeDefs from "./schema";
import resolvers from "./resolvers";
import typeResolvers from "../typeResolvers";
import { ApolloServer } from "apollo-server-express";

export default new ApolloServer({
  typeDefs,
  context: async ({ req }) => {
    return {
      ip: req.headers["x-forwarded-for"] || req.connection.remoteAddress
    };
  },
  resolvers: { ...resolvers, ...typeResolvers },
  playground: true
});
