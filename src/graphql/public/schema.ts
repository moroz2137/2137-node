const { gql } = require("apollo-server");
import postTypes from "../types/postTypes";
import commentTypes from "../types/commentTypes";

const typeDefs = gql`
  scalar Date
  scalar DateTime
  scalar JSON

  ${postTypes}
  ${commentTypes}

  type Query {
    hello: String!
    posts: [Post!]!
    comments(postId: ID!): [Comment!]!
    comment(id: ID!): Comment
  }

  type Mutation {
    createComment(params: CommentParams!): CommentMutationResponse!
  }

  input CommentParams {
    postId: ID!
    body: String
    signature: String
    website: String
  }

  type CommentMutationResponse {
    success: Boolean!
    comment: Comment
    errors: JSON
  }
`;

export default typeDefs;
