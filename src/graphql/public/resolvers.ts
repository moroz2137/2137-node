import Post from "../../models/Post";
import Comment from "../../models/Comment";
import { IResolverObject } from "graphql-tools";
import { ValidationError } from "sequelize/types";

function transformErrors({ errors }: ValidationError) {
  return errors.reduce((acc, val) => {
    const { path, message } = val;
    return {
      ...acc,
      [path]: message
    };
  }, {});
}

const resolverMap: IResolverObject = {
  Query: {
    hello: (_parent, _args) => {
      return "Hello, world!";
    },
    posts: (_parent, _args) => {
      return Post.all();
    },
    comments: (_parent, { postId }: any) => {
      return Comment.findAll({ where: { postId }, order: [["id", "DESC"]] });
    }
  },
  Mutation: {
    createComment: async (_parent, { params }: any, context) => {
      try {
        const comment = await Comment.create({ ...params, ip: context.ip });
        return {
          success: true,
          comment
        };
      } catch (err) {
        return {
          success: false,
          errors: transformErrors(err)
        };
      }
    }
  }
};

export default resolverMap;
