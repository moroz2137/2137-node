const { gql } = require("apollo-server");
import postTypes from "../types/postTypes";
import commentTypes from "../types/commentTypes";

const typeDefs = gql`
  scalar Date
  scalar DateTime
  scalar JSON

  ${postTypes}
  ${commentTypes}

  type Query {
    hello: String!
    posts(page: Int = 1): PostList!
    post(id: ID!): Post
    comments(postId: ID!): [Comment!]!
    comment(id: ID!): Comment
  }
`;

export default typeDefs;
