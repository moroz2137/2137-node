import Post from "../../models/Post";
import Comment from "../../models/Comment";
import { IResolverObject } from "graphql-tools";
import { ValidationError } from "sequelize/types";

function transformErrors({ errors }: ValidationError) {
  return errors.reduce((acc, val) => {
    const { path, message } = val;
    return {
      ...acc,
      [path]: message
    };
  }, {});
}

const resolverMap: IResolverObject = {
  Query: {
    hello: (_parent, _args) => {
      return "Hello, world!";
    },
    posts: async (_parent, { page }) => {
      const { count, totalPages, entries } = await Post.page(page);
      return {
        cursor: {
          totalEntries: count,
          totalPages,
          page
        },
        entries
      };
    },
    post: async (_parent, { id }) => {
      return Post.findBySlug(id);
    },
    comments: (_parent, { postId }: any) => {
      return Comment.findAll({ where: { postId }, order: [["id", "DESC"]] });
    }
  }
};

export default resolverMap;
