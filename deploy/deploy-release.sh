#!/bin/sh

set -e

git pull
yarn install
yarn run deploy
mkdir -p ../assets
rsync -r public/ ../assets
sudo service website restart
