const config = require("config");

module.exports = {
  development: config.get("dbConfig"),
  test: config.get("dbConfig"),
  production: config.get("dbConfig")
};
