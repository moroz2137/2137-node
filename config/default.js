function shortEnvName(env) {
  if (env === "production") return "prod";
  return "dev";
}

const env = shortEnvName(process.env.NODE_ENV);

module.exports = {
  secrets: {
    secretKeyBase:
      "u6sbQS9/2XZ63APNcv0aLRxUtp3rlm64WoRzbiLfz6GaPckiGXq2BK7QNis4qQfc"
  },
  session: {
    jwtSigner: "my_secret_key",
    maxAge: 12 * 3600 * 1000,
    secure: false
  },
  dbConfig: {
    database: `elefanto_${env}`,
    user: "postgres",
    password: "postgres",
    host: "/tmp",
    dialect: "postgres"
  },
  recaptcha: {
    publicKey: "my_public_key",
    secret: "my_secret",
    enable: false
  }
};
